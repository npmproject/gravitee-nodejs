const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const distPath = path.resolve('dist');
module.exports = {
	mode: 'production',
	target: 'node',
	entry: './src/index.js',
	output: {
		path: distPath,
		filename: 'index.js',
		libraryTarget: 'commonjs2',
	},
	module: {
		rules: [
			{
				test: /\.js?$/,
				exclude: /(node_modules)/,
				use: 'babel-loader',
			},
		],
	},
	plugins: [
		new CleanWebpackPlugin(),
		new CopyPlugin([{ from: './package.json', to: distPath }]),
		new CopyPlugin([{ from: './README.md', to: distPath }]),
	],
	resolve: {
		extensions: ['.js'],
	},
};
