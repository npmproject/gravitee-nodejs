# GRAVITEE-NODEJS

## What is it

gravitee-nodejs is npm module that allow you using configuration to communicate with your gravitee gateway.

## How to use

### To import the module

```
import { graviteeInstance } from '@obelghiti/gravitee-nodejs';
or
const { graviteeInstance } = require('@obelghiti/gravitee-nodejs');
```

### To initialize the configuration

```
graviteeInstance.config = {
  baseUrl: (GRAVITEE_GATEWAY_URL),
  clientDomaine: (GRAVITEE_CLIENT_DOMAINE),
  clientId: (GRAVITEE_CLIENT_ID),
  clientSecret: (GRAVITEE_CLIENT_SECRET)
};
```

Explanation of variables:

GRAVITEE_GATEWAY_URL=(url of you gateway)

GRAVITEE_CLIENT_DOMAINE=(domaine of your client)

GRAVITEE_CLIENT_ID=(the client id)

GRAVITEE_CLIENT_SECRET=(the client secret)

## What I can do with it

> Manage authentication (signIn/signOut/introSpect/userInfos/refreshToken)

> Manage users
