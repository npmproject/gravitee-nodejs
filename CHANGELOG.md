# [1.1.0](https://gitlab.com/npmproject/gravitee-nodejs/compare/v1.0.1...v1.1.0) / 2021-03-12

New Feature: Add function to delete user

# [1.0.1](https://gitlab.com/npmproject/gravitee-nodejs/compare/v1.0.0...v1.0.1) / 2020-06-02

Add possibility to use import and require with destructuring

# 1.0.0 / 2020-06-02

Init project
