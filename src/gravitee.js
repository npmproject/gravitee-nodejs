import axios from 'axios';

class Gravitee {
	set config(config) {
		this._config = config;
	}

	get config() {
		return this._config;
	}

	signIn({ username, password, scope = 'openid', isAdmin = false }) {
		const params = isAdmin
			? {}
			: {
					grant_type: 'password',
					username,
					password,
					scope,
			  };

		return axios.post(
			`${this._config.baseUrl}/${isAdmin ? 'admin' : `${this._config.clientDomaine}/oauth`}/token`,
			null,
			{
				params,
				auth: {
					username: isAdmin ? username : this._config.clientId,
					password: isAdmin ? password : this._config.clientSecret,
				},
			},
		);
	}

	signOut({ token }) {
		return axios.post(`${this._config.baseUrl}/${this._config.clientDomaine}/oauth/revoke`, null, {
			params: {
				token,
			},
			auth: {
				username: this._config.clientId,
				password: this._config.clientSecret,
			},
		});
	}

	introSpect({ token }) {
		return axios.post(
			`${this._config.baseUrl}/${this._config.clientDomaine}/oauth/introspect`,
			null,
			{
				params: {
					token,
				},
				auth: {
					username: this._config.clientId,
					password: this._config.clientSecret,
				},
			},
		);
	}

	getOpenIdConnectUserInfo({ type, token }) {
		return axios.get(`${this._config.baseUrl}/${this._config.clientDomaine}/oidc/userinfo`, {
			headers: {
				Authorization: `${type} ${token}`,
			},
		});
	}

	refreshToken({ refreshToken }) {
		const params = {
			grant_type: 'refresh_token',
			refresh_token: refreshToken,
		};

		return axios.post(`${this._config.baseUrl}/${this._config.clientDomaine}/oauth/token`, null, {
			params,
			auth: {
				username: this._config.clientId,
				password: this._config.clientSecret,
			},
		});
	}

	getUser({ type, token, idUser }) {
		return axios.get(
			`${this._config.baseUrl}/management/domains/${this._config.clientDomaine}/users/${idUser}`,
			{
				headers: {
					Authorization: `${type} ${token}`,
				},
			},
		);
	}

	getUsers({ type, token }, query) {
		return axios.get(
			`${this._config.baseUrl}/management/domains/${this._config.clientDomaine}/users`,
			{
				params: query,
				headers: {
					Authorization: `${type} ${token}`,
				},
			},
		);
	}

	getUserRoles({ type, token, idUser }) {
		return axios.get(
			`${this._config.baseUrl}/management/domains/${this._config.clientDomaine}/users/${idUser}/roles`,
			{
				headers: {
					Authorization: `${type} ${token}`,
				},
			},
		);
	}

	setUserRoles({ type, token, idUser }, user) {
		return axios.post(
			`${this._config.baseUrl}/management/domains/${this._config.clientDomaine}/users/${idUser}/roles`,
			user,
			{
				headers: {
					Authorization: `${type} ${token}`,
				},
			},
		);
	}

	createUsers({ type, token }, user) {
		return axios.post(
			`${this._config.baseUrl}/management/domains/${this._config.clientDomaine}/users`,
			user,
			{
				headers: {
					Authorization: `${type} ${token}`,
				},
			},
		);
	}

	deleteUser({ type, token, idUser }) {
		return axios.delete(
			`${this._config.baseUrl}/management/domains/${this._config.clientDomaine}/users/${idUser}`,
			{
				headers: {
					Authorization: `${type} ${token}`,
				},
			},
		);
	}

	updateUsers({ type, token, idUser }, user) {
		return axios.put(
			`${this._config.baseUrl}/management/domains/${this._config.clientDomaine}/users/${idUser}`,
			user,
			{
				headers: {
					Authorization: `${type} ${token}`,
				},
			},
		);
	}

	enableUsers({ type, token, idUser }, user) {
		return axios.put(
			`${this._config.baseUrl}/management/domains/${this._config.clientDomaine}/users/${idUser}/status`,
			user,
			{
				headers: {
					Authorization: `${type} ${token}`,
				},
			},
		);
	}

	resetPassword({ type, token, idUser }, user) {
		return axios.post(
			`${this._config.baseUrl}/management/domains/${this._config.clientDomaine}/users/${idUser}/resetPassword`,
			user,
			{
				headers: {
					Authorization: `${type} ${token}`,
				},
			},
		);
	}

	getRoles({ type, token }) {
		return axios.get(
			`${this._config.baseUrl}/management/domains/${this._config.clientDomaine}/roles`,
			{
				headers: {
					Authorization: `${type} ${token}`,
				},
			},
		);
	}
}

export default new Gravitee();
